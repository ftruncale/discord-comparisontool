#script for comparing two discord server's users

import discord
from discord.ext import commands #alt discord bot, more features? look into later
#import asyncio #so the bot can work while commands are needed?
import os
from os import path
import csv
import datetime #need to know when we saved a copy

bot = discord.ext.commands.Bot('>')

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    command_loop_main()

def compare_lists(s1_list, s2_list):
    matches = set(s1_list) & set(s2_list)

    print("[INFO] Users in both servers are as follows:")
    for id in matches:
        print(str(bot.get_user(id)))
    print("[INFO] There are %i matches." % (len(matches)))

    print("[INFO] Do you want to save the matches? (Y/N)")
    opt = "oof"
    while ( (opt != "Y") and (opt != "N")):
        opt = input()

    if (opt == "Y"):
        with open('matches.txt', 'w') as f:
            for id in matches:
                f.write("%s,%s\n" % (id, str(bot.get_user(id)).encode("ascii", errors="ignore").decode()))

    return

def generate_list(server_id):
    #i should probably slap these into classes eventually
    server = bot.get_guild(server_id)
    server_list = [member.id for member in server.members]

    return server_list

def save_local():
    server_id = server_select_actual()
    server = bot.get_guild(server_id)
    print("[INFO] Selected server is %s" % (server.name))
    print("[INFO] Do you want to save the members of the server? (Y/N)")
    opt = "oof"
    while ( (opt != "Y") and (opt != "N")):
        opt = input()

    if (opt == "Y"):
        member_list = generate_list(server_id)
        filename = (server.name).encode("ascii", errors="ignore").decode() + "_" + datetime.datetime.now().strftime("%Y%m%d_%H%M%S") +".txt"
        for char in '?/;:\\`|<>*': #server names can have funky characters a lot of oses don't like
            filename = filename.replace(char,'')
        with open(filename, 'w') as f:
            for id in member_list:
                f.write("%s,%s\n" % (id, str(bot.get_user(id)).encode("ascii", errors="ignore").decode()))
        print("[SUCCESS] File contains %i members and is saved as %s" % (len(member_list), filename))

    input("[INFO] Press ENTER to continue...")
    command_loop_main()
    return


def get_local():
    filename = ""

    print("[INFO] Enter filename! \n[NOTE] File must exist in local directory.\n")
    while True:
        try:
            filename = input()
            fh = open(filename)
        except FileNotFoundError:
            print("[ERROR] File %s not found! Try again." % (filename))

        else:
            print("[SUCCESS] File %s has been loaded." % (filename))
            break

    #whew, that file exists.
    rows = csv.reader(fh, delimiter=',')
    member_list = [int(row[0]) for row in rows]

    return(member_list)

def command_loop_main(): #additional functionality: compare to saved userlist
    os.system('cls' if os.name == 'nt' else 'clear')
    print("[INFO] Please select an option.")
    print("(0) Compare two servers")
    print("(1) Compare server to local list")
    print("(2) Save server to local list")
    opt = "oof"
    while ( (opt != "0") and (opt != "1") and (opt != "2")):
        opt = input()

    if (opt == "0"):
        server_select()

    elif (opt == "1"):
        compare_lists(get_local(), generate_list((server_select_actual())))

    elif (opt == "2"):
        save_local()

    command_loop_main()

def command_loop_servers(s1_id, s2_id):
    os.system('cls' if os.name == 'nt' else 'clear')
    print("[SERVERS] Your selected servers are %s and %s" % (bot.get_guild(s1_id).name, bot.get_guild(s2_id).name))
    print("[INFO] Please select an option.")
    print("(0) Compare users")
    print("(1) Reselect servers")
    print("(2) Main Menu")
    opt = "oof"
    while ( (opt != "0") and (opt != "1") and (opt != "2") ):
        opt = input()

    if (opt == "0"):
        compare_lists(generate_list(s1_id), generate_list(s2_id))
        command_loop_servers(s1_id, s2_id)
    elif (opt == "1"):
        server_select()
    else:
        command_loop_main()


def server_select_actual(): #does the work for the function below, also allows use elsewhere
    print("[INFO] Your available servers are:\n")
    server_ids = []
    i = 0
    for server in bot.guilds:
        server_ids.append([server.name, server.id])
        print("(%i) %s" % (i, server.name))
        i += 1

    print("\n[INFO] Select a server by (id).")
    checkval = int(input()) #typecast explicitly to int
    if ( (checkval < i) and (checkval >= 0) ):
        server_choice = checkval
    else:
        print("[ERROR] Invalid Choice. Try again.")

    return(server_ids[checkval][1])

def server_select():
    s1 = server_select_actual()
    print("[INFO] Please select another server.")
    s2 = server_select_actual()
    command_loop_servers(s1, s2) #pass through server ids


os.system('cls' if os.name == 'nt' else 'clear')
#check if token exists
if path.isfile("token.txt"):
    with open("token.txt") as f:
        token = f.readline()
    print("[INFO] Token found.")
    bot.run(token, bot=False)
else:
    print("[INFO] Please enter your discord account token:")
    token = input()
    print("[INFO] Saving token...")
    with open("token.txt", "w") as f:
        f.write(token)
    print("[INFO] Starting up and logging in...")
    bot.run(token, bot=False)
