# discord-comparisontool

A command line tool primarily meant to compare the users of separate discord servers.

Released under GPLv3.
