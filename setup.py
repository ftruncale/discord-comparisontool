# Setup script to create an executable

from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'includes': 'compat, asyncio, asyncio.compat, asyncio.base_futures, asyncio.base.tasks.base_tasks'
        'excludes': []
    }
}

executables = [
    Executable('discord_comparetool.py')
]

setup(name='Discord Comparison Tool',
      version='1.0',
      description='Assists in comparing the member lists of multiple Discord servers.',
      options=options,
      executables=executables
)
